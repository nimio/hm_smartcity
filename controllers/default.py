# -*- coding: utf-8 -*-

@cache.action(time_expire=3)
def index():

    session.forget(response)

    query_select = db.mastertable.ALL

    if request.args(0):

        query = (db.mastertable.casa_uuid == request.args(0))

        if request.args(1) and request.args(1, cast=int):
            limitby = (0,request.args(1, cast=int))

            if request.args(2):
                query_select = db.mastertable[request.args(2)]

        else:
            limitby = (0,100)
        

    else:
        limitby = (0,10)
        query = (db.mastertable.id > 0)

    dataset = db(query).select(query_select,
                               orderby =~ db.mastertable.momento|~db.mastertable.id,
                               limitby = limitby,
                               cacheable = True,
                               )

    if request.extension == 'json':
        response.headers['Access-Control-Allow-Origin'] = '*'
    
    return response.json({'data': dataset or EM('Sin datos')})



def casa():
    session.forget(response)

    if db(db.casa.id>0).isempty():
        db.casa.bulk_insert([
            {'casa_uuid': 1,
             'nombre': 'casa 1',
             'latitud': '-36.7603891',
             'longitud': '-73.09280899999999'
         },
            {'casa_uuid': 2,
             'nombre': 'casa 2',
             'latitud': '-36.97032508271402',
             'longitud': '-72.93203748144532'
         },
            {'casa_uuid': 3,
             'nombre': 'casa 3',
             'latitud': '36.9133173',
             'longitud': '-73.03261629999997'
         },
            {'casa_uuid': 4,
             'nombre': 'casa 4',
             'latitud': '-36.96406803832723',
             'longitud': '-73.15205697658234'
         },
            {'casa_uuid': 5,
             'nombre': 'casa 5',
             'latitud': '-36.9436689',
             'longitud': '-73.02096360000002l'
         },
            
        ])


    casa_uuid = request.args(0, cast=int)

    data = db(db.casa.casa_uuid == casa_uuid).select()
    
    if not data:
        data = db(db.casa.id>0).select()
    if request.extension == 'json':
        response.headers['Access-Control-Allow-Origin'] = '*'
        
    return response.json(data)



def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
