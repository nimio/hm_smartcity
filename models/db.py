# -*- coding: utf-8 -*-


db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
dbs = DAL('sqlite://scheduler.sqlite',pool_size=1,check_reserved=['all'])

'''
db = DAL('postgres://postgres:aoeu@localhost/smartcity', 
          #migrate=False, 
          pool_size=1,
          check_reserved=['all'])



dbs = DAL('postgres://postgres:aoeu@localhost/scheduler', 
          fake_migrate=True, 
          pool_size=1,
          check_reserved=['all'])
'''

response.generic_patterns = ['*'] if request.is_local else []

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False

auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

uuid_casas = xrange(1,10)


db.define_table('mastertable',
                Field('casa_uuid'),
                Field('momento', 'datetime',
                      compute = lambda r: request.now.now()),
                Field('temperatura'),
                Field('nivel_luz'),
                Field('consumo_electrico')
            )


db.define_table('casa',
                Field('casa_uuid',
                      requires = IS_IN_SET(uuid_casas)),
                Field('nombre'),
                Field('latitud'),
                Field('longitud'),
            )



def parser():
    '''
    Parser basico

    "puerto" corresponde al puerto de conexion del USB,
    eventualmente '/dev/ttyACM0' u otro por el estilo.
    Esta variable esta definida en el archivo no versionado
    ubicado en models/0_local.py
    '''


    import serial, random

    ser = serial.Serial(puerto, 9600)
    
    while True:
        
        # Filtro de datos
        data = eval(ser.readline())
                
        try:
            db.mastertable.insert(
                temperatura=data[0],
                nivel_luz=data[1],
                casa_uuid = random.choice(uuid_casas)
            )
            db.commit()
        except Exception as e:
            print(e)
            



def input_simulator():
    import random
    
    temp_max = 26
    temp_min = 11

    try:
        db.mastertable.bulk_insert([
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),
            dict(
                temperatura = random.uniform(temp_min, temp_max),
                nivel_luz = random.randrange(0, 150, 9),
                casa_uuid = random.choice(xrange(1,10)),
                consumo_electrico = random.uniform(5,10)
            ),

        ]
        )

        db.commit()
        print('Nuevo dato registrado')
    except Exception as e:
        print(e)

    

from gluon.scheduler import Scheduler
scheduler = Scheduler(dbs, heartbeat = 1)


if dbs(dbs.scheduler_task).isempty():

    '''
    scheduler.queue_task(
        function = parser,
        uuid = 'parser',
        start_time = request.now,
        timeout = 10,            
        prevent_drift=True,
        period=60,               
        immediate=True,
        repeats = 0,
        retry_failed = -1
    )
    print('Parser agendando')
    '''

    scheduler.queue_task(
        function = input_simulator,
        uuid = 'simulador',
        timeout = 1,            
        period = 1,
        prevent_drift = True,
        immediate=True,
        repeats = 0,
        retry_failed = -1
    )

    print('Simulador agendado')
